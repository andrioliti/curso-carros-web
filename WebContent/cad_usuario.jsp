<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de usuário</title>
</head>
<body>
	<h1>Cadastro de usuário</h1>	
	
	<form action="usuario">
		<table>
			<tr>
				<td>Nome</td>
				<td><input type="text" name="nome" required="required"/></td>
			</tr>
			<tr>
				<td>Sobrenome</td>
				<td><input type="text" name="sobrenome" required="required"/></td>
			</tr>
			<tr>
				<td>Idade</td>
				<td><input type="number" name="idade" min="0"/></td>
			</tr>
			<tr>
				<td><button type="submit">Enviar</button></td>
				<td><button type="reset">Limpar</button></td>
			</tr>
		</table>
	</form>
	
</body>
</html>