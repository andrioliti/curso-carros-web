<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="usuarioDao" class="main.com.carros_web.dao.UsuarioDAO"></jsp:useBean>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Usuário</title>
</head>
<body>
	<h1>Usuários</h1>
	
	<a href="cad_usuario.jsp">Cadastrar Usuário</a>
	
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Nome</th>
				<th>Idade</th>
				<th>Comandos</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="usuario" items="${usuarioDao.getUsuarios()}">
				<tr>
					<td>${usuario.codigo}</td>
					<td>${usuario.nome}</td>
					<td>${usuario.idade}</td>
					<td><a href="usuario/delete?codigo=${usuario.codigo}">excluir</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>