<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:useBean id="carroDao" class="main.com.carros_web.dao.CarroDAO"></jsp:useBean>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Carros</title>
</head>
<body>
	<h1>Carros</h1>	
	
	<table>
		<thead>
			<tr>
				<th>Id</th>
				<th>Modelo</th>
				<th>Valor</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="carro" items="${carroDao.getCarros()}">
				<tr>
					<td>${carro.codigo}</td>
					<td>${carro.valor}</td>
					<td>${carro.modelo}</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>