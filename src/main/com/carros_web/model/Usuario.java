package main.com.carros_web.model;

//essa é uma classe javabean
public class Usuario {

	public static final String codigo_ = "codigo";
	public static final String nome_ = "nome";
	public static final String sobrenome_ = "sobrenome";
	public static final String idade_ = "idade";
	public static final String tableName_ = "usuario";
	
	private Integer codigo;
	private String nome;
	private String sobrenome;
	private Integer idade;

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigo() {
		return this.codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}

	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	@Override
	public String toString() {
		return this.codigo + " - " + this.nome + " - "  + this.sobrenome + " - " + this.idade;
	}
	
}
