package main.com.carros_web.model;

import java.util.Date;

//essa é uma classe javabean pojo
public class Carro {

	public static final String codigo_ = "codigo";
	public static final String valor_ = "valor";
	public static final String modelo_ = "modelo";
	public static final String dataCompra_ = "data_compra";
	public static final String usuarioId_ = "usuario_id";
	public static final String tableName_ = "carro";
	
	private Integer codigo;
	private Float valor;
	private String modelo;
	private Date dataCompra;
	private Integer usuarioId;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Float getValor() {
		return valor;
	}

	public void setValor(Float valor) {
		this.valor = valor;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Date getDataCompra() {
		return dataCompra;
	}

	public void setDataCompra(Date dataCompra) {
		this.dataCompra = dataCompra;
	}

	public Integer getUsuarioId() {
		return usuarioId;
	}

	public void setUsuarioId(Integer usuarioId) {
		this.usuarioId = usuarioId;
	}

	public String toString() {
		return this.codigo + " - " + 
				this.modelo + " - " + 
				this.valor + " - " + 
				this.dataCompra + " - " + 
				this.usuarioId;
	}

}