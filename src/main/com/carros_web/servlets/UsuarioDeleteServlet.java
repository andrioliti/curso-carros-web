package main.com.carros_web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.com.carros_web.dao.UsuarioDAO;
import main.com.carros_web.model.Usuario;

@WebServlet("/usuario/delete")
public class UsuarioDeleteServlet extends HttpServlet {

	private static final long serialVersionUID = 327893919102328095L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String codigoParam = req.getParameter("codigo");
		
		UsuarioDAO dao = new UsuarioDAO();
		
		dao.deleteUsuario(Integer.valueOf(codigoParam));
		
		resp.sendRedirect(req.getContextPath() + "/lista_usuario.jsp");
	}
	
}
