package main.com.carros_web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/home")
public class FirstServlet extends HttpServlet {

	private static final long serialVersionUID = 9171246263830513197L;

	@Override
	public void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();

		out.print("<!DOCTYPE html>");
		out.print("<html lang=\"pt-BR\">");
		out.print("<head>");
		out.print("    <meta charset=\"UTF-8\">");
		out.print("    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
		out.print("    <title>Document</title>");
		out.print("</head>");
		out.print("<body>");
		out.print("<h1>Carros e usuários</h1>"
				+"<ul>"
				+ "<li><a href=\"usuarios\">Usuarios</a></li>"
				+ "</ul>");
		out.print("</body>");
		out.print("</html>");
	}

}
