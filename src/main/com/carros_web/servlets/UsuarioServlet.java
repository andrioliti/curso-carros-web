package main.com.carros_web.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.com.carros_web.dao.UsuarioDAO;
import main.com.carros_web.model.Usuario;

@WebServlet("/usuario")
public class UsuarioServlet extends HttpServlet {

	private static final long serialVersionUID = 327893919102328095L;
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String nomeParam = req.getParameter("nome");
		String sobrenomeParam = req.getParameter("sobrenome");
		String idadeParam = req.getParameter("idade");
		
		UsuarioDAO dao = new UsuarioDAO();
		
		Usuario usuario = new Usuario();
		
		usuario.setNome(nomeParam);
		usuario.setSobrenome(sobrenomeParam);
		usuario.setIdade(Integer.valueOf(idadeParam));
		
		dao.insertUsuario(usuario);
		
		RequestDispatcher requestDispatcher = 
				req.getRequestDispatcher("lista_usuario.jsp");
		
		requestDispatcher.forward(req, resp);
	}
	
}
