package main.com.carros_web.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FabricaConexao {
	
	private static FabricaConexao instance = null;
	private Connection con;
	
	private FabricaConexao() {
		for (;;) {
			System.out.println("Estou tentando me conectar");
			try {
				this.con = DriverManager.getConnection("jdbc:postgresql://127.0.0.1:5432/postgres?user=curso&password=12345");
				break;
			} catch (SQLException e) {
				System.out.println("Erro ao tentar se conectar com o banco de dados");
				System.out.println(e.getMessage());
				e.printStackTrace();
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static FabricaConexao getInstance() {
		if (instance == null) {
			instance = new FabricaConexao();
		}
		
		return instance;
	}
	
	public Connection getConnection() {
		return this.con;
	}
}
