
package main.com.carros_web;

import java.util.List;

import main.com.carros_web.dao.UsuarioDAO;
import main.com.carros_web.model.Usuario;

public class Main {

	public static void imprimir(List list) {
		System.out.println("\n############");
		list.stream().forEach(System.out::println);
	}
	
	public static void main(String[] args) {
		UsuarioDAO usuarioDAO = new UsuarioDAO();
		
		List<Usuario> usuarios = usuarioDAO.getUsuarios();
		imprimir(usuarios);
		
		Usuario leo = new Usuario();
		
		leo.setIdade(20);
		leo.setNome("Leo");
		leo.setSobrenome("Nascimento");
		
		usuarioDAO.insertUsuario(leo);
		
		usuarios = usuarioDAO.getUsuarios();
		imprimir(usuarios);
		
		leo = usuarios.get(usuarios.size() - 1);
		leo.setIdade(26);
		
		usuarioDAO.updateUsuario(leo);
		
		usuarios = usuarioDAO.getUsuarios();
		imprimir(usuarios);
		
		usuarioDAO.deleteUsuario(leo.getCodigo());
		
		usuarios = usuarioDAO.getUsuarios();
		imprimir(usuarios);
	}
		
}
