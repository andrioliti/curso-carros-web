package main.com.carros_web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import main.com.carros_web.model.Carro;
import main.com.carros_web.utils.FabricaConexao;

public class CarroDAO {
	
	private static final String SELECT_STATEMENT = "SELECT * FROM " + Carro.tableName_;
	
	private Connection con;
	
	public CarroDAO() {
		this.con = FabricaConexao.getInstance().getConnection();
	}
	
	public List<Carro> getCarros() {
		List<Carro> carros = new LinkedList<>();
		PreparedStatement stm;
		ResultSet resDb;
		
		try {
			stm = con.prepareStatement(SELECT_STATEMENT);
			resDb = stm.executeQuery();
		
			
			while (resDb.next()) {
				Carro carro = new Carro();
				
				carro.setCodigo(resDb.getInt(Carro.codigo_));
				carro.setModelo(resDb.getString(Carro.modelo_));
				carro.setValor(resDb.getFloat(Carro.valor_));
				carro.setDataCompra(resDb.getDate(Carro.dataCompra_));
				carro.setUsuarioId(resDb.getInt(Carro.usuarioId_));
				
				carros.add(carro);
			}
			
			stm.close();
			resDb.close();
		} catch (SQLException e) {
			System.out.println("Erro ao se conectar com o banco de dados");
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		return carros;
	}
	
}	
