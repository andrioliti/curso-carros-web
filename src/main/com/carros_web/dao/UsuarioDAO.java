package main.com.carros_web.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import main.com.carros_web.model.Usuario;
import main.com.carros_web.utils.FabricaConexao;

public class UsuarioDAO {

	private final String SELECT_STATEMENT = "SELECT * FROM " + Usuario.tableName_;
	private final String INSERT_STATMENTE = "INSERT INTO " + Usuario.tableName_ + " (" +
			Usuario.nome_ + ", " +
			Usuario.sobrenome_ + ", " +
			Usuario.idade_ + ") VALUES (?,?,?)";
			
	private final String UPDATE_STATMENTE = "UPDATE "+ Usuario.tableName_ +
			" SET "+ Usuario.nome_ +"=?," +
			Usuario.sobrenome_+"=?," +
			Usuario.idade_ +"=?" + 
			"WHERE "+ Usuario.codigo_ +"=?";
	
	private final String  DELETE_STATMENTE= "DELETE FROM "+ Usuario.tableName_ +
			" WHERE "+ Usuario.codigo_ +"=?";

	private Connection con;
	
	public UsuarioDAO()  {
		con = FabricaConexao.getInstance().getConnection();
	}
	
	public List<Usuario> getUsuarios() {
		List<Usuario> usuarios = new LinkedList<>();
		
		try {
			PreparedStatement stm = con.prepareStatement(SELECT_STATEMENT);
			ResultSet resDb = stm.executeQuery();
			
			while (resDb.next()) {
				Usuario usuario = new Usuario();
				
				usuario.setCodigo(resDb.getInt(Usuario.codigo_));
				usuario.setNome(resDb.getString(Usuario.nome_));
				usuario.setSobrenome(resDb.getString(Usuario.sobrenome_));
				usuario.setIdade(resDb.getInt(Usuario.idade_));
				
				usuarios.add(usuario);
			}
			
			stm.close();
			resDb.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
		
		return usuarios;
	}
	
	public void insertUsuario(Usuario usuario) {
		try {
			PreparedStatement stm = con.prepareStatement(INSERT_STATMENTE);
			
			stm.setString(1, usuario.getNome());
			stm.setString(2, usuario.getSobrenome());
			stm.setInt(3, usuario.getIdade());
			
			stm.execute();
			
			stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void updateUsuario(Usuario usuario) {
		try {
			PreparedStatement stm = con.prepareStatement(UPDATE_STATMENTE);
			
			stm.setString(1, usuario.getNome());
			stm.setString(2, usuario.getSobrenome());
			stm.setInt(3, usuario.getIdade());
			stm.setInt(4, usuario.getCodigo());
			
			stm.executeUpdate();
			
			stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteUsuario(Integer codigo) {
		try {
			PreparedStatement stm = con.prepareStatement(DELETE_STATMENTE);
			
			stm.setInt(1, codigo);
			
			stm.execute();
			
			stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
